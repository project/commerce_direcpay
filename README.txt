-- SUMMARY --

Commerce DirectPay is a [Drupal
Commerce](https://drupal.org/project/commerce) module that integrates
the [DirecPay](http://www.direcpay.com/direcpay/home.jsp) payment gateway into
 your Drupal Commerce shop.

-- REQUIREMENTS --

To use this module you need to signup as a merchant with DirecPay. To know more
how to signup, please visit
http://www.direcpay.com/direcpay/how-to-become-merchant.jsp
Once you are a merchant with DirecPay, you will receive a merchant id, which
will be used on your Drupal commerce store.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* To change the Merchant Id and payment mode
  (Test transactions or Live transactions), Go to
  Store settings > Payment methods admin/commerce/config/payment-methods,
  Look for DirecPay from the available payment methods and click on edit link.
  Under "Enable payment method: DirecPay" Action, click on the edit link. Enter
  Merchant Id and Select Payment mode as Live transactions for DirecPay to
  accept real payments.

-- CONTACT --
Sagar Rammgade (https://www.drupal.org/user/399718)
